const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (req, res) => {
	
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	return newUser.save()
	.then((user) => res.status(201).send({ message: "Registered Successfully"}));
}

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.status(404).send({ error: "No Email Found" });
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password); 
			if (isPasswordCorrect == true) {

				return res.status(200).send({ access : auth.createAccessToken(result)})
			} else {
				return res.status(401).send({ message: "Email and/or password do not match" });
			}
		}
	})
}

module.exports.getAllUsers = (req, res) => {
	return User.find({})
	.then(result => {
		res.status(200).send({result});
	})
}

module.exports.getProfile = (req, res) => {
	
	// req.user is the payload of the token upon login
	console.log("req.user display:");
	console.log(req.user);

	// return User.findOne({_id: req.user.id})
	return User.findById(req.user.id)
	.then(result =>{
		// validation
		if(!result){
				// status code - 404
			return res.status(404).send({error: 'User not found'});
		}
		else{
			result.password = "*****";
				// // status code - 200
			return res.status(200).send({result})
		}
	})	
}

// module.exports.resetPassword = async (req, res) => {
// 	try {
// 	  const { newPassword } = req.body; // Extracting newPassword from the request body
// 	  const { userId } = req.user; // Extracting user ID from the URL params
  
// 	  // Hashing the new password
// 	  const hashedPassword = await bcrypt.hash(newPassword, 10);
  
// 	  // Updating the user's password in the database
// 	  await User.findByIdAndUpdate(userId, { password: hashedPassword });
  
// 	  // Sending a success response
// 	  res.status(200).send({ message: 'Password reset successfully' });
// 	} catch (error) {
// 	  console.error(error);
// 	  res.status(500).send({ message: 'Internal server error' });
// 	}
//   };


module.exports.resetPassword = async (req, res) => {
	try {
	  const newPassword = { password : bcrypt.hashSync(req.body.newPassword, 10)};
  
	  await User.findByIdAndUpdate(req.user.id, newPassword);
  
	  res.status(200).send({ message: 'Password updated successfully' });
	} catch (error) {
	  console.error(error);
	  res.status(500).send({ message: 'Internal server error' });
	}
  };

  module.exports.updateUserToAdmin = async (req, res) => {
	try {
	  const { userId } = req.params;
  
	  // Check if the requesting user is an admin
	  if (!req.user.isAdmin) {
		return res.status(403).send({ message: 'Permission denied. Only admins can update user roles.' });
	  }
  
	  // Update the user's role to admin
	  const updatedUser = await User.findByIdAndUpdate(userId, { isAdmin: true });
  
	  // Check if the user was found and updated
	  if (!updatedUser) {
		return res.status(404).send({ message: 'User not found.' });
	  }
  
	  res.status(200).send({ message: 'User role updated successfully.' });
	} catch (error) {
	  console.error('Error updating user role:', error);
	  res.status(500).send({ message: 'Failed to update user role.' });
	}
  }; 