const Product = require("../models/product");

module.exports.getAllProduct = (req, res) => {
	return Product.find({})
	.then(result => {
		res.status(200).send({result});
	})
};

module.exports.getAllActiveProduct = (req, res)=>{
	return Product.find({isActive: true})
	.then(product => {
		if(product.length > 0){
			return res.status(200).send({product});
		}
		else{
			return res.status(200).send({message: `No active products found.`})
		}

		})
	.catch(err => res.status(500).send({ error: `Error finding active products`}));

};

//Add a post
module.exports.addProduct = (req, res) => {
	const { name, description, price } = req.body;
  
	Product.findOne({ name })
	  .then(existingProduct => {
		if (existingProduct) {
		  return res.status(409).send({ error: "Product already exists" });
		}
  
		let newProduct = new Product({
		  name: req.body.name,
		  description: req.body.description,
		  price: req.body.price,
		});
  
		return newProduct
		  .save()
		  .then(savedProduct => res.status(201).send({ savedProduct }))
		  .catch(err => {
			console.error("Error in saving the product", err);
			return res.status(500).send({ error: "Failed to save the product" });
		  });
	  })
	  .catch(err => {
		console.error("Error in checking for existing product", err);
		return res.status(500).send({ error: "Failed to check for existing product" });
	  });
  };

module.exports.getProduct = (req, res) => {
	Product.findById(req.params.productId)
	.then(product => {
		if (!product) {
			return res.status(404).send({error: "Product not found"})
		}
		else{
			return res.status(200).send({product})
		}
	})
	.catch(err => {
		 console.error("Error in retrieving the product", err);
		 return res.status(500).send({error: 'Failed to fetch product'});
	})
}


module.exports.archiveProduct = (req, res) => {
	let archivedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(req.params.productId, archivedProduct)
	.then(archiveProduct => {
		if(!archiveProduct){
			return res.status(404).send({ error: 'Product not found' });
		}
		else{
			return res.status(200).send(
				{ 
	        	message: 'Product archived successfully', 
	        	archivedProduct: archiveProduct 
	        	}
	        );
		}
	})
	.catch(err => {
		console.error("Error in updating a product: ", err)
		return res.status(500).send({ error: 'Error in updating a prodcut.' });
	});
}

module.exports.activateProduct= (req, res) => {
  let activatedProduct = {
  	isActive:true
  }
  return Product.findByIdAndUpdate(req.params.productId, activatedProduct)
	.then(activateProduct => {
		if(!activateProduct){
			return res.status(404).send({ error: 'Product not active' });
		}
		else{
			return res.status(200).send(
				{ 
	        	message: 'Product activate successfully', 
	        	activatedProduct: activateProduct
	        	}
	        );
		}
	}).catch(err => {
		console.error("Error in updating a product: ", err)
		return res.status(500).send({ error: 'Error in updating a product.' });
	});
}

module.exports.updateProduct = (req, res) => {
    let updatedProduct = {
        name: req.body.name, // new name
        description: req.body.description,  // new description
        price: req.body.price // new price
    }
    return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then(result => {
        if(!result){
            return res.status(404).send({ error: 'Product not found' });
        }
        else{
            return res.status(200).send(
                { 
                message: 'Product updated successfully', 
                updatedProduct: updatedProduct 
                }
            );
        }
    })
}

module.exports.searchProductsByPrice = (req, res) => {
  const { minPrice, maxPrice } = req.body;

  // Validate input parameters
  if (minPrice === undefined || maxPrice === undefined) {
    return res.status(400).send({ error: 'minPrice and maxPrice are required in the request body' });
  }

  // Query the database for courses within the given price range
  Product.find({
    price: { $gte: minPrice, $lte: maxPrice },
  })
    .then(product => res.status(200).send({ product }))
    .catch(error => res.status(500).send({ error: 'Error searching products by price range' }));
};


module.exports.searchProductsByName = async (req, res) => {
	try {
	  const { name } = req.body;
  
	  // Validate input parameter
	  if (!name) {
		return res.status(400).send({ error: 'Name is required in the request body' });
	  }
  
	  // Case-insensitive search for courses by name
	  const product = await Product.find({ name: { $regex: new RegExp(name, 'i') } });
  
	  if (product.length === 0) {
		return res.status(404).send({ message: 'No products found with the provided name' });
	  }
  
	  res.status(200).send({ product });
	} catch (error) {
	  console.error(error);
	  res.status(500).send({ error: 'Error products by name' });
	}
}