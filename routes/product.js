const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.post("/", verify, verifyAdmin, productController.addProduct);

router.get("/all",verify,verifyAdmin,productController.getAllProduct);

router.get("/", productController.getAllActiveProduct);

router.get("/:productId", productController.getProduct);

router.put("/:productId/update",verify, verifyAdmin, productController.updateProduct);

router.patch("/:productId/archive",verify, verifyAdmin, productController.archiveProduct );

router.patch("/:productId/activate", verify, verifyAdmin,productController.activateProduct);

router.post ("/searchByPrice", productController.searchProductsByPrice);

router.post ("/searchByName", productController.searchProductsByName);




module.exports = router; 

// router.post('/search', productController.searchProductByPrice);