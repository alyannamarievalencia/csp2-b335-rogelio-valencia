const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

router.post("/", userController.registerUser);

router.post("/login", userController.loginUser);	

router.get("/all", verify, verifyAdmin, userController.getAllUsers);

router.get("/details", verify, userController.getProfile);

router.post('/update-password', verify, userController.resetPassword);

router.put('/:userId/set-as-admin', verify, verifyAdmin, userController.updateUserToAdmin);

module.exports = router;